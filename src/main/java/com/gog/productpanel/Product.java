/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waraporn.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author Acer
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    
    public static ArrayList<Product> genProductList(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "Hot Latte", 30, "1.jpg"));
        list.add(new Product(2, "Ice Latte", 40, "2.jpg"));
        list.add(new Product(3, "Mocca", 45, "3.jpg"));
        list.add(new Product(4, "Americano", 45, "4.jpg"));
        list.add(new Product(5, "Espresso", 40, "5.jpg"));
        list.add(new Product(6, "Cocoa", 45, "6.jpg"));
        list.add(new Product(7, "Thai Tea", 35, "7.jpg"));
        list.add(new Product(8, "Green Tea", 30, "8.jpg"));
        list.add(new Product(9, "Fresh Milk", 35, "9.jpg"));
        return list;
    }
}
